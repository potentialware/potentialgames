package potentialgames.controller;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import potentialgames.data.Cell;
import potentialgames.data.ICell;
import potentialgames.data.IPlayer;
import potentialgames.data.Player;

public class TestSetPoI extends SetPoI {

	static IPlayer[] plrs;
	static ICell[] way;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		plrs = new Player[10];
		way = new Cell[2];
	}

	@Test
	public void testCalcEntry() {
		assertEquals("SetPoI", false, SetPoI.calcEntry(plrs, way));
		plrs = new Player[4];
		way = new Cell[4];
		assertEquals("SetPoI", false, SetPoI.calcEntry(plrs, way));
		
	}

}
