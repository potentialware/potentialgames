package potentialgames.controller;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;


public class TestGameController {

	static GameController gc;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gc = new GameController();
	}
	@Test
	public void testGetPlayercount() {
		gc.setPlayercount(2);
		assertEquals("getplayercount", 2, gc.getPlayercount());
	}
	@Test
	public void testGetNames() {
		String[] n = {"a", "b"};
		gc.setNames(n);
		assertEquals("GetSetNames", "a", gc.getNames()[0]);
		assertEquals("GetSetNames", "b", gc.getNames()[1]);
		gc.setName(0, "bla");
		assertEquals("GetSetNames", "bla", gc.getNames()[0]);
	}

	@Test
	public void testGetName() {
		assertEquals("GetSetNames", "Player", gc.getName(0));
	}

	@Test
	public void testGetBots() {
		boolean[] b ={true, false};
		gc.setBots(b);
		gc.setBot(0, false);
		assertEquals("getbots", false, gc.getBots()[0]);
		assertEquals("getbots", false, gc.getBots()[1]);
	}

	@Test
	public void testGetBot() {
		assertEquals("getSetBot", false, gc.getBot(1));
	}

	@Test
	public void testGetPincount() {
		gc.setPincount(5);
		assertEquals("SetGetPincount",5,gc.getPincount());
	}

	@Test
	public void testGetField() {
		gc.setFields(15);
		assertEquals("GetSEtGamefields",15,gc.getFields());
	}
	@Test
	public void testGameInit(){
		gc.gameInit();
		assertEquals("getWayLenght", 15, gc.getWay().length);
		assertEquals("getField", 15, gc.getField().getWay().length);
		assertEquals("getWin", false, gc.getWin());
		assertEquals("getPlayer", 5, gc.getPlayer(0).getPincount());
		
	}
	
	/*
	@Test
	public void testMove(){
		assertEquals("pinmove1",true,gc.pinMove(0).equals("Move done"));
		assertEquals("pinmove2",false,gc.pinMove(0).equals("Move done"));
	}
	*/
	@Test
	public void testAmZug(){
		int i = gc.getAmZug();
		assertEquals("amzug",0,i);
		gc.next();
		assertEquals("amzug",1,gc.getAmZug());
		gc.next();
		assertEquals("amzug",0,gc.getAmZug());
	}
	@Test
	public void steps(){
		gc.setRolled(true);
		assertNotNull(gc.getRolled());
		assertNotNull(gc.roll());
	}
	
	
}
