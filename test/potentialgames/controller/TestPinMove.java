package potentialgames.controller;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;


public class TestPinMove {

	static GameController gc;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gc = new GameController();
		gc.setPlayercount(2);
		String[] n = {"a", "b"};
		gc.setNames(n);
		boolean[] b ={true, false};
		gc.setBots(b);
		gc.setPincount(5);
		gc.setFields(10);
		gc.gameInit();
	}
	
	@Test
	public void testPinActionMove(){
		
		PinAction action = new PinActionMove();
		assertEquals("pinentry1",false,action.pinEntry(gc, gc.getPlayer(0), 0));
		
		assertEquals("pinentry2",true,action.pinEntry(gc, gc.getPlayer(0), 6));
		
		assertEquals("pinentry3",true,action.pinMove(gc, gc.getPlayer(0), 0, 2));
		
		assertEquals("pinentry3",true,action.pinMove(gc, gc.getPlayer(0), 0, 7));
		
		gc.getField().getWay()[gc.getPlayer(0).getEntry()].setPlayer(gc.getPlayer(1));
		gc.getField().getWay()[gc.getPlayer(0).getEntry()].setPin(gc.getPlayer(1).getPin(0));
		assertEquals("pinentry4",true,action.pinEntry(gc, gc.getPlayer(0), 6));
		
		gc.getField().getWay()[gc.getPlayer(0).getEntry()].setPlayer(gc.getPlayer(0));
		gc.getField().getWay()[gc.getPlayer(0).getEntry()].setPin(gc.getPlayer(0).getPin(3));
		assertEquals("pinentry4",true,action.pinEntry(gc, gc.getPlayer(0), 6));
		
		assertEquals("pinentry3",true,action.pinMove(gc, gc.getPlayer(0), 0, 15));
		
	}	
}
