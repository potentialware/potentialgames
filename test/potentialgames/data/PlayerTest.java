package potentialgames.data;

import static org.junit.Assert.*;

import java.util.ArrayDeque;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

import potentialgames.data.Player;


public class PlayerTest {
	IPlayer testPlayer1;
	Queue<IPin> pins;
	IPin testPin;
	@Before
	public void setUp() throws Exception {
		pins = new ArrayDeque<IPin>();
		testPlayer1 = new Player("testName", true, 1, pins);
	}

	@Test
	public void testGetPlayerName() {
		assertEquals("PlayerOrganism", "testName", testPlayer1.getPlayerName());
	}
	@Test
	public void testGetOrganism() {
		assertEquals("PlayerOrganism", true, testPlayer1.getOrganism());
	}
	@Test
	public void testGetEntryandExit() {
		testPlayer1.setEntry(5);
		testPlayer1.setExt(4);
		assertEquals("getentry failed", 5, testPlayer1.getEntry());
		assertEquals("getentry failed", 4, testPlayer1.getExt());
	}
	@Test
	public void testGetPin() {
		char b = 'b';
		testPlayer1.setPinColor(b);

		assertEquals("getpin failed", 1, testPlayer1.getPincount());
		
		assertEquals("getpin failed", b, testPlayer1.getPinColor());
	}
	@Test
	public void testPinPos(){
		testPlayer1.setPinPos(0,5);
		assertEquals("getpinpos", 5, testPlayer1.getPinPos(0));
		assertEquals("getpin", 5, testPlayer1.getPin(0).getCellNumber());
	}
	@Test
	public void testPlrNr(){
		testPlayer1.setPlayerNr(1);
		assertEquals("getPlayerNr", 1, testPlayer1.getPlayerNr());
	}

}
