package potentialgames.data;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class GameFieldTest {

	static GameField myField;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		myField = new GameField(5, 1);
	}

	@Test
	public void testGetWay() {
		assertEquals("GetWay ERR", 5,myField.getWay().length);
	}

	@Test
	public void testGetHome() {
		assertEquals("GetHome ERR", 0,myField.getHome(0).size());
	}
	@Test
	public void testGetTarget() {
		assertEquals("GetHome ERR", 0,myField.getTarget(0).size());
	}

}
