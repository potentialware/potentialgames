package potentialgames.data;

import static org.junit.Assert.*;
import potentialgames.data.Config;
import org.junit.Before;
import org.junit.Test;

public class ConfigTest {
	Config cfg = new Config();
	@Before
	public void setUp() throws Exception {
		String[] bla = {"bla","blubb"};
		boolean[] bot = {false, true};
		cfg.setNames(bla);
		cfg.setName(0, "name");
		cfg.setPlayercount(1);
		cfg.setFields(1);
		cfg.setBots(bot);
		cfg.setBot(0, true);
		cfg.setPincount(1);
	}

	@Test
	public void testGetNames() {
		assertEquals("GetNames() ERR", "name", cfg.getNames()[0]);
	}

	@Test
	public void testGetBots() {
		assertEquals("getbots() ERR", true, cfg.getBots()[0]);
	}

	@Test
	public void testGetPlayercount() {
		assertEquals("GetPlayercount() ERR", 1, cfg.getPlayercount());
	}

	@Test
	public void testGetFields() {
		assertEquals("Getfields() ERR", 1, cfg.getFields());
	}

	@Test
	public void testGetPincount() {
		assertEquals("GetPincount() ERR", 1, cfg.getPincount());
	}

}
