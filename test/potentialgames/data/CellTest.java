package potentialgames.data;

import static org.junit.Assert.*;

import java.util.ArrayDeque;

import org.junit.Before;
import org.junit.Test;

import potentialgames.data.Cell;
import potentialgames.data.Pin;
import potentialgames.data.Player;


public class CellTest {

	ICell testCell;
	IPlayer testPlr;
	IPin testPin;
	@Before
	public void setUp() throws Exception {
		testCell = new Cell();
		
		testPlr = new Player("Name",true,5, new ArrayDeque<IPin>());
		testPin = new Pin();
		testCell.setCell(testPlr, testPin, 0);
	}
	@Test
	public void testSetCell() {
		testCell.setCell(testPlr, testPin, 0);
		assertEquals("cellset is wrong", testPlr, testCell.getPlayer());
		assertEquals("cellset is wrong", testPin, testCell.getPin());
	}

	@Test
	public void testSetXY() {
		testCell.setXY(1, 1);
		assertEquals("cellset is wrong", 1, testCell.getx());
		assertEquals("cellset is wrong", 1, testCell.gety());
	}


	@Test
	public void testSetPin() {
		testCell.setPin(testPin);
		assertEquals("cellset is wrong", testPin, testCell.getPin());
	}
	
	@Test
	public void testSetPlayer() {
		testCell.setPlayer(testPlr);
		assertEquals("setplayer fails", testPlr, testCell.getPlayer());
	}

}
