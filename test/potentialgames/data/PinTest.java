package potentialgames.data;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import potentialgames.data.Pin;



public class PinTest {

	Pin testpin;
	
	@Before
	public void setUp() throws Exception {
		testpin = new Pin();
	}
	@Test
	public void testSetCellNumber(){
		testpin.setCellNumber(1);
		assertEquals("callnumber is wrong", 1, testpin.getCellNumber());
	}
	@Test
	public void testSetInhouse(){
		testpin.setInhouse(false);
		assertEquals("setInHouse ERR", false, testpin.getInhouse());
	}
	@Test
	public void testSetHome(){
		testpin.setInTarget(true);
		assertEquals("setInHouse ERR", true, testpin.getInTarget());
	}
	@Test
	public void testSetPinNr(){
		testpin.setPinNr(1);
		assertEquals("setpinnr", 1, testpin.getPinNr());
	}
}
