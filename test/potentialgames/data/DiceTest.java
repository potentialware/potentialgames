package potentialgames.data;

import static org.junit.Assert.*;

import org.junit.Test;

import potentialgames.data.Dice;


public class DiceTest extends Dice {

	@Test
	public void testRoll() {
		int i = Dice.roll();
		assertEquals("rolling fails", true, i < 7 || i > 0);
	}

}
