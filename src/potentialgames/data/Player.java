package potentialgames.data;

import java.util.Queue;

/**
 * A class to create new Playerobjects.
 */
public class Player implements IPlayer{
	
	private static String playerName;
	private static boolean bot;
	
	private IPin[] pinArray;
	
	private int entry;
	private int ext;
	private char color;
	private int pincount;
	private int playernr;
	
	
	/**
	 * Constructor for new Playerobjects
	 * @param s String of the Playername.
	 * @param b boolean for bot or human.
	 * @param playerNr the playernumber. have to be between 0,1,2 or 3.
	 */
	public Player(String s, boolean b, int amountOfPins, Queue<IPin> house) {
		playerName = s;
		bot = b;
		pinArray = new Pin[amountOfPins];
		setPinArray(house);
		pincount = amountOfPins;
	}
	/**
	 * Fills the House and the Pincontainer with the Pins
	 * @param house
	 */
	private void setPinArray(Queue<IPin> house){
		for(int i = 0; i < pinArray.length; i++){
			pinArray[i] = new Pin();
			pinArray[i].setPinNr(i);
			house.add(pinArray[i]);
		}
	}

	/**
	 * Get the playername
	 * @return Sting: Playername.
	 */
	public String getPlayerName() {
		return playerName;
	}
	/**
	 * Get the info: "bot or human"
	 * @return true if human, false if bot.
	 */
	public boolean getOrganism() {
		return bot;
	}
	/**
	 * get one of the pins of player
	 * @param i number of the pin(0-3)
	 * @return returns the pin object.
	 */
	public IPin getPin(int i) {
		return pinArray[i];
	}
	/**
	 * 
	 * @param e new Entry
	 */
	public void setEntry(int e){
		entry = e;
	}
	/**
	 * 
	 * @param e new exit value
	 */
	public void setExt(int e){
		ext = e;
	}
	/**
	 * get the entypoint
	 * @return int (entrypoint on way)
	 */
	public int getEntry(){
		return entry;
	}
	/**
	 * get exitpoint
	 * @return int (exitpoint on way)
	 */
	public int getExt(){
		return ext;
	}
	/**
	 * set new color
	 * @param x new color
	 */
	public void setPinColor(char x) {
		color = x;
	}
	/**
	 * get pincolor
	 * @return char of the color.
	 */
	public char getPinColor(){
		return color;
	}
	

	public int getPincount(){
		return pincount;
	}

	public int getPinPos(int i){
		return pinArray[i].getCellNumber();
	}

	public void setPinPos(int i, int newpos){
		pinArray[i].setCellNumber(newpos);
	}
	public int getPlayerNr() {
		return playernr;
	}
	public void setPlayerNr(int x) {
		playernr = x;
		
	}
}
