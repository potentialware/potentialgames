package potentialgames.data;
/**
 * A class for creating random numbers between 0 and 7.
 *
 */
public abstract class Dice {
	/**
	 * Create a random number.
	 * @return integer between 0 and 7.
	 */
	//To avoid  magic numbers NARF
	private static final int HUNDRET_NUM = 100;
	private static final int SIX_NUM = 6;
	/**
	 * Get a new random number between [0,7] 
	 * @return Integer [0,7]
	 */
	public static int roll(){
		double random = Math.random();
		random = random * HUNDRET_NUM;
		random = (random % SIX_NUM) +1;
		return (int)random;
	}
}
