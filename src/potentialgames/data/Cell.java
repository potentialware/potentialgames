package potentialgames.data;
/**
 * Implementation of the ICell.
 * Just only cell elements on the gamefield
 * @author Michael Schettler
 *
 */
public class Cell implements ICell{

	private IPlayer player;
	private IPin pin;
	private int x;
	private int y;

	
	
	/**
	 * 
	 * @param plr new Player
	 * @param p new Pin
	 * @param a new Position for Pin
	 */
	public void setCell(IPlayer plr, IPin p, int a) {
		player = plr;
		pin = p;
		pin.setCellNumber(a);

	}

	/**
	 * 
	 * @return returns the new Player
	 */
	public IPlayer getPlayer() {
		return player;
	}
	
	public void setPlayer(IPlayer p){
		player = p;
	}

	/**
	 * 
	 * @return returns the Pin on the Cell
	 */
	public IPin getPin() {
		return pin;
	}

	/**
	 * 
	 * @param a new value for x
	 * @param b new value for b
	 */
	public void setXY(int a, int b) {
		x = a;
		y = b;
	}

	/**
	 * 
	 * @return returns x value
	 */
	public int getx() {
		return x;
	}

	/**
	 * 
	 * @return returns y value
	 */
	public int gety() {
		return y;
	}
	/**
	 * 
	 * @param p new Pin
	 */
	public void setPin(IPin p){
		pin=p;
	}


}
