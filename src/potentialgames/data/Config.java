package potentialgames.data;

import java.util.Arrays;


public class Config{

	private int playercount;
	private int fields;
	private String[] names;
	private boolean[] bots;
	private int pincount;
		
	// Getter
	
	public String[] getNames(){
		return names;
	}
	
	public boolean[] getBots(){
		return bots;
	}
	
	public int getPlayercount(){
		return playercount;
	}
	
	public int getFields(){
		return fields;
	}
	
	public int getPincount(){
		return pincount;
	}
	
	// Setter
	
	public void setNames(String[] n){
		names = Arrays.copyOf(n, n.length);
	}
	
	public void setName(int i, String n){
		names[i] = n;
	}
	
	public void setPlayercount(int i){
		playercount = i;
	}
	
	public void setFields(int f){
		fields = f;
	}
	
	public void setBots(boolean b[]){
		bots = Arrays.copyOf(b, b.length);
	}
	
	public void setBot(int i, boolean b){
		bots[i] = b;
	}
	
	public void setPincount(int p){
		pincount = p;
	}
	

}
