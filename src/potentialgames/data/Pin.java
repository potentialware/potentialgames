package potentialgames.data;

/**
 * Class for Pin Objectives.
 */
public class Pin implements IPin{
	private int cellNumber;
	private boolean inhouse = true;
	private boolean inhome = false;
	private int pinnr;

	/**
	 * get The CallNumber
	 * @return returns the actual position Number
	 */
	public int getCellNumber(){
		return cellNumber;
	}
	
	/**
	 * Set the Cell Number.
	 * @param a set a new position number
	 */
	public void setCellNumber(int a){
		cellNumber = a;
	}

	/**
	 * returns player is in house
	 * @return true for in house 
	 */
	public boolean getInhouse(){
		return inhouse;
	}

	/**
	 * sets inhouse
	 * @param i bool inhouse
	 */
	public void setInhouse(boolean i){
		inhouse = i;
	}
	
	/**
	 * sets inhome
	 * @param h boolean inhome
	 */
	public void setInTarget(boolean h){
		inhome = h;
	}
	
	/**
	 * returns player is in inhome
	 * @return returns true if in home
	 */
	public boolean getInTarget(){
		return inhome;
	}

	public int getPinNr() {
		return pinnr;
	}

	public void setPinNr(int x) {
		pinnr = x;
		
	}
	
	
	
	
}
