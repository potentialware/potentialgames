package potentialgames.data;
/**
 * Interface for Pinobjects
 * @author Michael Schettler
 *
 */
public interface IPin {
	int getCellNumber();
	
	/**
	 * Set the Cell Number.
	 * @param a set a new position number
	 */
	void setCellNumber(int a);

	
	int getPinNr();
	void setPinNr(int x);
	/**
	 * returns player is in house
	 * @return true for in house 
	 */
	boolean getInhouse();

	/**
	 * sets inhouse
	 * @param i bool inhouse
	 */
	void setInhouse(boolean i);
	
	/**
	 * sets inhome
	 * @param h boolean inhome
	 */
	void setInTarget(boolean h);

	
	/**
	 * returns player is in inhome
	 * @return returns true if in home
	 */
	boolean getInTarget();
}
