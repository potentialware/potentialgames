package potentialgames.data;

public interface IPlayer {
	

	/**
	 * Get the playername
	 * @return Sting: Playername.
	 */
	String getPlayerName();
	/**
	 * Get the info: "bot or human"
	 * @return true if human, false if bot.
	 */
	
	int getPlayerNr();
	void setPlayerNr(int x);
	boolean getOrganism();
	/**
	 * get one of the pins of player
	 * @param i number of the pin(0-3)
	 * @return returns the pin object.
	 */
	IPin getPin(int i);
	/**
	 * 
	 * @param e new Entry
	 */
	void setEntry(int e);
	/**
	 * 
	 * @param e new exit value
	 */
	void setExt(int e);
	/**
	 * get the entypoint
	 * @return int (entrypoint on way)
	 */
	int getEntry();
	/**
	 * get exitpoint
	 * @return int (exitpoint on way)
	 */
	int getExt();
	/**
	 * set new color
	 * @param x new color
	 */
	void setPinColor(char x);
	/**
	 * get pincolor
	 * @return char of the color.
	 */
	char getPinColor();
	/**
	 * get the amount of pins
	 * @return Integer (amount of pins)
	 */
	int getPincount();
	/**
	 * get the position of one pin
	 * @param i the number of the pin
	 * @return Integer position on the gamefield
	 */
	int getPinPos(int i);

	/**
	 * Set a new position to the pin i
	 * @param i the number of the pin
	 * @param newpos Integer ( new position)
	 */
	void setPinPos(int i, int newpos);
}
