package potentialgames.data;

import java.util.Queue;

public interface IGameField {

	/**
	 * Get the logic way array
	 * @return array of cells
	 */
	ICell[] getWay();
	/**
	 * get the home of a player
	 * @param playerNumber number of player
	 * @return Queue<IPin> of the Home
	 */
	Queue<IPin> getHome(int playerNumber);	
	
	/**
	 * get the Targetarray
	 * @param playerNumber number of the player
	 * @return Cellarray of the targets
	 */
	Queue<IPin> getTarget(int playerNumber);
}
