package potentialgames.controller;

import java.util.Observable;
import potentialgames.controller.PinActionMove;
import potentialgames.data.Config;
import potentialgames.data.Dice;
import potentialgames.data.GameField;
import potentialgames.data.ICell;
import potentialgames.data.IGameField;
import potentialgames.data.IPlayer;
import potentialgames.data.Player;

/**
 * Class for new Gamecontroller.
 */
public class GameController extends Observable {
	public static int ERR_DOUBLE_ROLL = -6;
	public static int ERR_NO_PINS_IN_HOME = -7;
	public static int ERR_NO_ROLLED_SIX_TO_ENTRY = -8;
	public static int ERR_PIN_NOT_ON_FIELD = -9;
	public static int ERR_SAME_PLAYER_ON_FIELD = -10;
	public static int ERR_NOT_ROLLED = -11;

	static final int SIX = 6;
	private IGameField fld;
	private IPlayer[] plr;
	private int amZug = 0;
	private int rollTrys;
	private int steps;
	private PinAction pa;
	private boolean rolled;
	private String returnarg = "";

	private boolean win = false;
	private Config cfg;

	public GameController() {

		cfg = new Config();
	}

	public void gameInit() {
		fld = new GameField(getFields(), getPlayercount());
		pa = new PinActionMove();
		// Create player and set color

		plr = new Player[getPlayercount()];
		char[] color = { 'b', 'y', 'r', 'g' };
		for (int i = 0; i < getPlayercount(); ++i) {
			plr[i] = new Player(getName(i), getBot(i), getPincount(),
					fld.getHome(i));
			// Set Players color. b=blue, y=yellow, r=red, g=green
			plr[i].setPinColor(color[i]);
			plr[i].setPlayerNr(i);
		}
		// TODO catch error(false)
		SetPoI.calcEntry(plr, fld.getWay());

		// All params are initialized lets play
		setChanged();
		notifyObservers("Game Init Done...");
		this.rollTrys = 0;
		this.win = false;
	}

	/* config getter */
	public String[] getNames() {
		return cfg.getNames();
	}

	public String getName(int i) {
		// String[] tmp = cfg.getNames()
		// eturn tmp[i]
		return "Player";
	}

	public boolean[] getBots() {
		return cfg.getBots();
	}

	public boolean getBot(int i) {
		// boolean[] tmp = cfg.getBots()
		// return tmp[i]
		return false;
	}

	public int getPlayercount() {
		return cfg.getPlayercount();
	}

	public int getFields() {
		return cfg.getFields();
	}

	public int getPincount() {
		return cfg.getPincount();
	}

	public IGameField getField() {
		return fld;
	}

	public ICell[] getWay() {
		return fld.getWay();
	}

	public boolean getWin() {
		return win;
	}

	public IPlayer getPlayer(int i) {
		return plr[i];
	}

	public int getAmZug() {
		return amZug;
	}

	public int getSteps() {
		return steps;
	}

	public boolean getRolled() {
		return rolled;
	}

	/* config setter */
	public void setRolled(boolean a) {
		rolled = a;
	}

	public void setNames(String[] n) {
		cfg.setNames(n);
	}

	public void setName(int i, String n) {
		cfg.setName(i, n);
	}

	public void setPlayercount(int i) {
		cfg.setPlayercount(i);
	}

	public void setFields(int f) {
		cfg.setFields(f);
	}

	public void setBots(boolean b[]) {
		cfg.setBots(b);
	}

	public void setBot(int i, boolean b) {
		cfg.setBot(i, b);
	}

	public void setPincount(int p) {
		cfg.setPincount(p);
	}

	public void setReturnarg(String s) {
		returnarg = s;
	}

	/* functional methods */
	public int roll() {
		if ((this.getField().getHome(amZug).size() + this.getField()
				.getTarget(amZug).size()) == this.getPincount()) {
			if (this.rollTrys < 3) {
				steps = Dice.roll();
				this.rollTrys++;
				setChanged();
				notifyObservers("rolled " + steps);
				return steps;
			} else {
				setRolled(true);
				setChanged();
				notifyObservers("rolled " + steps);
				return ERR_DOUBLE_ROLL;
			}
		} else {
			if (this.rolled) {
				return ERR_DOUBLE_ROLL;
			} else {
				steps = Dice.roll();
				setRolled(true);
				setChanged();
				notifyObservers("rolled " + steps);
				return steps;
			}
		}
	}

	public int pinMove(int pin) {
		// wenn roll = 6 und home nicht leer --> entry

		// wenn pin auf feld dann
		if (fld.getTarget(amZug).contains(plr[amZug].getPin(pin))) {
			return ERR_PIN_NOT_ON_FIELD;
		} else {
			if(steps == 0 || !rolled){
				return ERR_NOT_ROLLED;
			}
			if (pa.pinMove(this, plr[amZug], pin, this.steps)) {
				steps = 0;
				setChanged();
				notifyObservers(returnarg);
				checkWin();
				return 0;
			} else {
				return ERR_SAME_PLAYER_ON_FIELD;
			}

		}
	}

	public int pinEntry() {
		if (steps != 6) {
			return ERR_NO_ROLLED_SIX_TO_ENTRY;
		} else if (fld.getHome(amZug).size() == 0) {
			return ERR_NO_PINS_IN_HOME;
		} else {
			pa.pinEntry(this, plr[amZug], steps);
			rollTrys = 0;
			steps = 0;
			setRolled(false);
			setChanged();
			notifyObservers(returnarg);
			return 0;
		}
	}

	public void next() {
		amZug++;
		setRolled(false);
		steps = 0;
		this.rollTrys = 0;
		if (amZug == this.getPlayercount()) {
			amZug = 0;
		}
		setChanged();
		notifyObservers("next");
	}

	private void checkWin() {

		if (fld.getHome(amZug).size() == 0
				&& fld.getTarget(amZug).size() == this.getPincount()) {
			this.win = true;
		}
	}

}
