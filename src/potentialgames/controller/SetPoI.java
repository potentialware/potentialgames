package potentialgames.controller;

import potentialgames.data.ICell;
import potentialgames.data.IPlayer;

public abstract class SetPoI {
	public static boolean calcEntry(IPlayer[] plr, ICell[] way){
		int entry = 1;
		if(way.length%plr.length != 0){
			return false;
		}
		int distance = (way.length/plr.length);
		if(distance < 2){
			return false;
		}
		for(int i = 0; i < plr.length; i++){
			plr[i].setEntry(entry);
			plr[i].setExt(entry-1);
			entry += distance;
		}
		return true;
	}	
}
