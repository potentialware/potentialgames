package potentialgames.controller;

import potentialgames.data.IPlayer;

public interface PinAction {
	boolean pinMove(GameController gc, IPlayer player, int pin, int steps);
	boolean pinEntry(GameController gc, IPlayer player, int steps);
}