package potentialgames;


import static java.lang.System.in;

import java.util.Scanner;

import org.apache.log4j.PropertyConfigurator;

import potentialgames.aview.tui.Tui;
import potentialgames.controller.GameController;

public final class Potentialgames {

	private static Potentialgames instance;
	private GameController controller;
	
	public GameController getController() {
		return controller;
	}


	public static Potentialgames getInstance(){
		if (instance == null) instance =  new Potentialgames();
		return instance;
	}
	
	
	private Potentialgames(){
		// ================================================================================
		PropertyConfigurator.configure("log4j.properties");
		controller = new GameController();
		// Call GUI or TUI
		tui = new Tui(controller); 
		controller.gameInit();
		//new Gui(controller);
	}
	
	// pg.tui.gethtml
	
	private static Tui tui;
	
	public static Tui getTui() {
		return tui;
	}

	
	/**
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		while(!Potentialgames.getInstance().getController().getWin()){
			tui.tuihandler(new Scanner(in).nextLine());
		}
	}


}
