package potentialgames.aview.tui;

import org.apache.log4j.Logger;

import potentialgames.controller.GameController;
import static java.lang.System.out;


/**
 * Klasse zum einlesen der Config. Sie schreibt 
 * @author Slaster
 *
 */
public abstract class GameConfig {
	static final Logger LOGGER=Logger.getLogger("potentialgames.aview.GameConfig");
	static final int TWENTY = 20;
	public static void createConfig(GameController gc){
		// ================================================================================
		
		
		// Get Number of fields:
		LOGGER.info("Enter the Number of Fields : ");
		//gc.setFields (EINGABE.nextInt())	
		gc.setFields(TWENTY);
		LOGGER.info("You choosed " +gc.getFields() + " Fields\n");
		
		// Get Number of pins:
		LOGGER.info("Enter the Number of Pins : ");
		//gc.setPincount (EINGABE.nextInt())
		gc.setPincount(1);
		LOGGER.info("You choosed " +gc.getPincount() +  " Pins\n");
		
		// Get player count:
		LOGGER.info("Enter the Number of drunk players: ");
		//gc.setPlayercount (EINGABE.nextInt())
		gc.setPlayercount(2);
		if (gc.getPlayercount() == 0) {
			out.println("fuuuuu 0 players aren't enough\n");
			System.exit(1);
		}
		LOGGER.info("You choosed "+ gc.getPlayercount() + " Players\n");

		// Get playernames and bots
		gc.setNames(new String[gc.getPlayercount()]);
		gc.setBots(new boolean[gc.getPlayercount()]);
		
		for (int i = 0; i < gc.getPlayercount(); ++i) {
			LOGGER.info("Enter name for Player " + i);
			//gc.setName (i, EINGABE.next())
			gc.setName(i, "Spielername");
			LOGGER.info("is that player a Bot? (true, false)");
			//gc.setBot (i, EINGABE.nextBoolean())
			gc.setBot(i, true);
		}
		
		
	}
	
}
