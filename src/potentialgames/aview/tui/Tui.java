package potentialgames.aview.tui;

import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.*;

import potentialgames.controller.GameController;

public class Tui implements Observer {
	private GameController gc;
	static final Logger LOGGER=Logger.getLogger("potentialgames.aview.tui");
	public Tui(GameController gcu) {
		gc = gcu;
		LOGGER.info("Wuhuuu lets play MADN");

		// Create config
		GameConfig.createConfig(gc);
		gc.addObserver(this);
		
	}

	public void tuihandler(String input) {

		String[] inputsplit;
		// =================================================================================
		// get input

		inputsplit = input.split(" ");
		LOGGER.info(input);
		if (inputsplit[0].equals("h")) {
			LOGGER.info("Help");
			LOGGER.info("Controlls:\n\t-w: W�rfeln\n\t-m <int> movepin\n\t-n: next player\n\t-e: pin entry\n\t-q: quit game\n\t-s: starts the game.\n");
		}

		if (inputsplit[0].equals("w")) {
			int roll = gc.roll();
			if(roll == GameController.ERR_DOUBLE_ROLL){
				redraw("Double rolling not permitted!");
			}
		}

		if (inputsplit[0].equals("e")) {
			int status = gc.pinEntry();

			if(status == GameController.ERR_NO_PINS_IN_HOME){
				redraw("no pins in home");
			} else if(status == GameController.ERR_SAME_PLAYER_ON_FIELD){
				redraw("can not enter, own pin on field");
			} else if(status == 0){				
				redraw("pin entered");
			}
		}
		
		if (inputsplit[0].equals("n")) {
			gc.next();
			redraw("please roll");
		}
		

		if (inputsplit[0].equals("q")) {
			LOGGER.info("Game exited\n\n\n\n");
			System.exit(0);
		}

		if (inputsplit[0].equals("s")) {
			LOGGER.info("Starting game");
			gc.gameInit();
		}

		if (inputsplit[0].equals("m")) {
			// TODO eingabesicherheit pr�fen

			if (gc.getRolled()) {
				/* start pin move */
				gc.pinMove(Integer.parseInt(inputsplit[1]));
			} else {
				LOGGER.info("Zuerst Wuerfeln!");
			}
			gc.setRolled(false);
		}
	}

	private void redraw(Object arg) {
		// Malen
		LOGGER.info("\n");
		LOGGER.info("You can roll? " + !gc.getRolled() + "\n");
		LOGGER.info(arg);
		String x1 = "Player " + (gc.getAmZug() + 1);
		LOGGER.info(x1);
		
		
		
		// Draw homes
		StringBuilder x2 = new StringBuilder();
		for (int i = 0; i < gc.getPlayercount(); i++) {
			x2.append("|"+(i + 1) +gc.getField().getHome(i).size() + "|\t");
			
		}
		LOGGER.info(x2);
		
		// draw way
		StringBuilder x3 = new StringBuilder();
		for (int i = 0; i < gc.getWay().length; i++) {
			if (gc.getWay()[i].getPlayer() != null) {
				x3.append("|" + (gc.getWay()[i].getPlayer().getPlayerNr() + 1) + gc.getWay()[i].getPin().getPinNr());
				
			} else {
				x3.append("|--");
			}
		}
		x3.append("|");
		LOGGER.info(x3);
		// draw targets
		StringBuilder x4 = new StringBuilder();
		for (int i = 0; i < gc.getPlayercount(); i++) {
			x4.append("|" + (i + 1) + gc.getField().getTarget(i).size() + "|\t"); 
			
		}
		LOGGER.info(x4);
		LOGGER.info("");

	}
	
	public String drawHTML(Object arg) {
		// Malen
		
		LOGGER.info("\n");
		LOGGER.info("You can roll? " + !gc.getRolled() + "\n");
		LOGGER.info(arg);
		String x1 = "Player " + (gc.getAmZug() + 1);
		LOGGER.info(x1);
		
		
		
		// Draw homes
		StringBuilder x2 = new StringBuilder();
		for (int i = 0; i < gc.getPlayercount(); i++) {
			x2.append("|"+(i + 1) +gc.getField().getHome(i).size() + "|\t");
			
		}
		LOGGER.info(x2);
		
		// draw way
		StringBuilder x3 = new StringBuilder();
		for (int i = 0; i < gc.getWay().length; i++) {
			if (gc.getWay()[i].getPlayer() != null) {
				x3.append("|" + (gc.getWay()[i].getPlayer().getPlayerNr() + 1) + gc.getWay()[i].getPin().getPinNr());
				
			} else {
				x3.append("|--");
			}
		}
		x3.append("|");
		LOGGER.info(x3);
		// draw targets
		StringBuilder x4 = new StringBuilder();
		for (int i = 0; i < gc.getPlayercount(); i++) {
			x4.append("|" + (i + 1) + gc.getField().getTarget(i).size() + "|\t"); 
			
		}
		LOGGER.info(x4);
		LOGGER.info("");
		return "madn html tui";

	}

	public void update(Observable o, Object arg) {
		redraw(arg);
		

	}

}
