package potentialgames.aview.gui;

/**
 * 
 * @author Sl4ster
 * Just a Numberclass to make metric happy
 */
public abstract class Numbers {
	public static final int TAUSENVIERUNDZWANZIG = 1024;
	public static final int ACHTHUNDERTVIERUNDSECHZIG = 864;
	public static final int ACHTHUNDERT = 800;
	public static final int SIEBENHUNDERTZEHN = 710;
	public static final int SECHSHUNDERTNEUNZIG = 690;
	public static final int SECHSHUNDERTNEUNUNDZWANZIG = 629;
	public static final int FUENFHUNDERFUENFZIG = 550;
	public static final int VIERHUNDERTFUENFZEHN = 415;
	public static final int DREIHUNDERTSIEBENSUNDSECHSIG = 367;
	public static final int DREIHUNDERFUENFZIG = 350;
	public static final int	DREIHUNDERTEINUNDVIERZIG = 341;
	public static final int ZWEIHUNDERTFUENFUNDNEUNZIG = 295;
	public static final int ZWEIHUNDERTACHTUNDFUENZIG = 285;
	public static final int ZWEIHUNDERTFUENZIG = 250;
	public static final int ZWEIHUNDERTZWANZIG = 220;
	public static final int HUNDERTACHZIG = 180;
	public static final int HUNDERT = 100;
	public static final int FUENFUNDNEUNZIG = 95;
	public static final int NEUNZIG = 90;
	public static final int SIEBZIG = 70;
	public static final int FUENFZIG = 50;
	public static final int DREISSIG = 30;
	public static final int ZWANZIG = 20;
	public static final int ACHZEN = 18;
	public static final int ELF = 11;
	public static final int ZEHN = 10;
	public static final int VIER = 4;
	public static final int DREI = 3;
}
