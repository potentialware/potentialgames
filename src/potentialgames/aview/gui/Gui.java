package potentialgames.aview.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.*;
import potentialgames.aview.gui.Numbers;
import potentialgames.controller.GameController;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

public class Gui implements ActionListener, Observer {
	private GameController gc;
	static final int PLAYERNAMELENGTH = 20;

	private JFrame frame;

	private JMenuItem start;
	private JMenuItem exit;
	private JRadioButtonMenuItem plr2;
	private JRadioButtonMenuItem plr3;
	private JRadioButtonMenuItem plr4;
	private JRadioButtonMenuItem pin1;
	private JRadioButtonMenuItem pin2;
	private JRadioButtonMenuItem pin3;
	private JRadioButtonMenuItem pin4;
	private JTextField fields;
	private static JButton dice;
	private static JButton logo;
	private JPanel panel;
	private JLabel rollresult;
	private Point[] points;
	private JButton[] posbutton;
	private JButton home1;
	private JButton home2;
	private JButton home3;
	private JButton home4;
	private JButton target1;
	private JButton target2;
	private JButton target3;
	private JButton target4;
	private String font = "Calibri";
	private JButton nextbtn;
	private JLabel msg;

	public Gui(GameController gcue) {
		gc = gcue;

		gc.gameInit();
		frame = new JFrame("MADN RockZ");
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		/* Menu Bar */
		JMenu file = new JMenu("File");
		file.setMnemonic(KeyEvent.VK_F);
		JMenu config = new JMenu("Config");
		config.setMnemonic(KeyEvent.VK_C);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setForeground(Color.YELLOW);

		/* File Menu */
		// TODO Start Game
		// TODO Exit
		start = new JMenuItem("Start Game");
		exit = new JMenuItem("Exit...");
		start.setMnemonic(KeyEvent.VK_S);
		exit.setMnemonic(KeyEvent.VK_E);
		start.addActionListener(this);
		exit.addActionListener(this);

		/* Config Menu */
		ButtonGroup playergroup = new ButtonGroup();
		plr2 = new JRadioButtonMenuItem("2 Player");
		plr3 = new JRadioButtonMenuItem("3 Player");
		plr4 = new JRadioButtonMenuItem("4 Player");

		ButtonGroup pingroup = new ButtonGroup();
		pin1 = new JRadioButtonMenuItem("1 Pin");
		pin2 = new JRadioButtonMenuItem("2 Pin");
		pin3 = new JRadioButtonMenuItem("3 Pin");
		pin4 = new JRadioButtonMenuItem("4 Pin");
		JLabel field = new JLabel("Fields");
		fields = new JTextField("" + Numbers.ZWANZIG);

		/* Aktions */
		plr2.addActionListener(this);
		plr3.addActionListener(this);
		plr4.addActionListener(this);
		pin1.addActionListener(this);
		pin2.addActionListener(this);
		pin3.addActionListener(this);
		pin4.addActionListener(this);
		fields.addActionListener(this);

		plr2.setSelected(true);
		playergroup.add(plr2);
		playergroup.add(plr3);
		playergroup.add(plr4);
		pin1.setSelected(true);
		pingroup.add(pin1);
		pingroup.add(pin2);
		pingroup.add(pin3);
		pingroup.add(pin4);

		/* Build Menus */
		file.add(start);
		file.add(exit);

		/* Config Menu */
		config.add(plr2);
		config.add(plr3);
		config.add(plr4);
		config.addSeparator();
		config.add(pin1);
		config.add(pin2);
		config.add(pin3);
		config.add(pin4);
		config.addSeparator();
		config.add(field);
		config.add(fields);

		/* Build Menubar */
		menuBar.add(file);
		menuBar.add(config);

		frame.setResizable(false);
		frame.setPreferredSize(new Dimension(Numbers.TAUSENVIERUNDZWANZIG,
				Numbers.ACHTHUNDERT));
		frame.getContentPane().setBackground(Color.ORANGE);

		panel = new JPanel();
		panel.setBackground(Color.ORANGE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		drawallblah("Init");

		drawGamefield();

		frame.getContentPane().add(panel);

		frame.pack();
		frame.setVisible(true);

		gc.addObserver(this);
	}

	public void actionPerformed(ActionEvent e) {
		Object q = e.getSource();
		if (q.equals(plr2)) {
			gc.setPlayercount(2);
		}
		if (q.equals(plr3)) {
			gc.setPlayercount(Numbers.DREI);
		}
		if (q.equals(plr4)) {
			gc.setPlayercount(Numbers.VIER);
		}
		if (q.equals(pin1)) {
			gc.setPincount(1);
		}
		if (q.equals(pin2)) {
			gc.setPincount(2);
		}
		if (q.equals(pin3)) {
			gc.setPincount(Numbers.DREI);
		}
		if (q.equals(pin4)) {
			gc.setPincount(Numbers.VIER);
		}
		if (q.equals(fields)) {
			int input = 0;
			try {
				input = Integer
						.parseInt(((JTextField) e.getSource()).getText());
			} catch (NumberFormatException e2) {

			}

			gc.setFields(input);

			frame.repaint();
		}
		if (q.equals(exit)) {
			System.exit(0);
		}
		if (q.equals(start)) {
			gc.gameInit();
			redraw("Start Game...");
			frame.invalidate();

			frame.repaint();

		}
		if (q.equals(dice)) {
			if (!gc.getRolled()) {
				gc.roll();
				rollresult.setText("You rolled  " + gc.getSteps());
			} else {
				redraw("Double rolling not permitted!");
			}
		}
		doMove(q);
		
		
		if (q.equals(nextbtn)) {
			gc.next();
		}

	}
	private void doMove(Object q){
		for (int i = 0; i < gc.getFields(); i++) {
			try {
				if (q.equals(posbutton[i])
						&& gc.getField().getWay()[i].getPin() != null) {
					gc.pinMove(gc.getField().getWay()[i].getPin().getPinNr());
				}
			} catch (ArrayIndexOutOfBoundsException eb) {

			}

		}
		if (q.equals(home1)) {
			gc.pinEntry();
		}
		if (q.equals(home2)) {
			gc.pinEntry();
		}
		if (q.equals(home3)) {
			gc.pinEntry();
		}
		if (q.equals(home4)) {
			gc.pinEntry();
		}
	}

	private Point getMiddle() {
		Point middlepoint;
		int hight = Numbers.TAUSENVIERUNDZWANZIG;
		int width = Numbers.ACHTHUNDERT;

		middlepoint = new Point();
		middlepoint.setLocation(hight / 2, width / 2 - Numbers.FUENFZIG);
		return middlepoint;
	}

	private Point[] calcpos(Point middlepoint) {
		double angle = 2 * Math.PI / gc.getFields();
		int radius = Numbers.DREIHUNDERFUENFZIG;
		points = new Point[gc.getFields()];
		for (int i = 0; i < gc.getFields(); i++) {
			points[i] = new Point();
			points[i].x = (int) (Math.cos(((double) i) * angle) * radius)
					+ middlepoint.x;
			points[i].y = (int) (Math.sin(((double) i) * angle) * radius)
					+ middlepoint.y;

		}
		return points;
	}

	private void drawbuttons() {
		posbutton = new JButton[gc.getFields()];
		for (int i = 0; i < gc.getFields(); i++) {
			posbutton[i] = new JButton();

			if (gc.getPlayer(0).getEntry() == i) {
				posbutton[i].setBackground(Color.red);
			}
			if (gc.getPlayer(1).getEntry() == i) {
				posbutton[i].setBackground(Color.yellow);
			}
			if (gc.getPlayercount() >= Numbers.DREI
					&& gc.getPlayer(2).getEntry() == i) {
				posbutton[i].setBackground(Color.blue);
			}

			if (gc.getPlayercount() == Numbers.VIER
					&& gc.getPlayer(Numbers.DREI).getEntry() == i) {
				posbutton[i].setBackground(Color.green);
			}
			if (gc.getField().getWay()[i].getPlayer() != null) {
				if (gc.getField().getWay()[i].getPlayer().getPlayerNr() == 0) {
					posbutton[i].setSelectedIcon(new ImageIcon(Gui.class
							.getResource("/potentialgames/pinred.png")));
					posbutton[i].setIcon(new ImageIcon(Gui.class
							.getResource("/potentialgames/pinred.png")));
					posbutton[i].repaint();
				}
				if (gc.getField().getWay()[i].getPlayer().getPlayerNr() == 1) {
					posbutton[i].setSelectedIcon(new ImageIcon(Gui.class
							.getResource("/potentialgames/pinyellow.png")));
					posbutton[i].setIcon(new ImageIcon(Gui.class
							.getResource("/potentialgames/pinyellow.png")));
				}
				if (gc.getField().getWay()[i].getPlayer().getPlayerNr() == 2) {
					posbutton[i].setSelectedIcon(new ImageIcon(Gui.class
							.getResource("/potentialgames/pinblue.png")));
					posbutton[i].setIcon(new ImageIcon(Gui.class
							.getResource("/potentialgames/pinblue.png")));
				}
				if (gc.getField().getWay()[i].getPlayer().getPlayerNr() == Numbers.DREI) {
					posbutton[i].setSelectedIcon(new ImageIcon(Gui.class
							.getResource("/potentialgames/pingreen.png")));
					posbutton[i].setIcon(new ImageIcon(Gui.class
							.getResource("/potentialgames/pingreen.png")));
				}

			}
			posbutton[i].setBounds(points[i].x, points[i].y, Numbers.DREISSIG,
					Numbers.DREISSIG);
			posbutton[i].addActionListener(this);
			posbutton[i].revalidate();
			posbutton[i].repaint();
			panel.add(posbutton[i]);

		}

	}

	private void drawGamefield() {
		calcpos(getMiddle());
		drawbuttons();
	}

	private void redraw(Object arg) {
		panel.removeAll();
		drawallblah(arg);
		drawGamefield();
		panel.revalidate();
		panel.repaint();
	}

	private void drawallblah(Object arg) {
		JLabel whosturn;
		whosturn = new JLabel("P " + (gc.getAmZug() + 1) + "'s turn");
		whosturn.setFont(new Font(font, Font.PLAIN, Numbers.ACHZEN));
		whosturn.setBounds(Numbers.HUNDERTACHZIG, Numbers.SECHSHUNDERTNEUNZIG,
				Numbers.HUNDERT, Numbers.FUENFZIG);
		panel.add(whosturn);

		msg = new JLabel((String) arg);
		msg.setHorizontalAlignment(SwingConstants.CENTER);
		msg.setFont(new Font(font, Font.PLAIN, Numbers.ACHZEN));
		msg.setBounds(Numbers.DREIHUNDERTEINUNDVIERZIG, Numbers.FUENFHUNDERFUENFZIG, Numbers.DREIHUNDERFUENFZIG, Numbers.FUENFZIG);
		panel.add(msg);

		rollresult = new JLabel("");
		rollresult.setFont(new Font(font, Font.PLAIN, Numbers.ACHZEN));
		rollresult.setBounds(Numbers.SIEBZIG, Numbers.SECHSHUNDERTNEUNZIG,
				Numbers.HUNDERT, Numbers.FUENFZIG);
		panel.add(rollresult);

		dice = new JButton("");
		dice.setBackground(Color.ORANGE);
		dice.setBounds(Numbers.ZEHN, Numbers.SECHSHUNDERTNEUNZIG,
				Numbers.FUENFZIG, Numbers.FUENFZIG);
		dice.setSelectedIcon(new ImageIcon(Gui.class
				.getResource("/potentialgames/wuerfel.png")));
		dice.setToolTipText("Click to roll");
		dice.setIcon(new ImageIcon(Gui.class
				.getResource("/potentialgames/wuerfel.png")));
		dice.setPreferredSize(new Dimension(Numbers.FUENFZIG, Numbers.FUENFZIG));
		dice.setBorderPainted(false);
		dice.setContentAreaFilled(false);
		dice.addActionListener(this);
		panel.add(dice);

		nextbtn = new JButton("");
		nextbtn.setSelectedIcon(new ImageIcon(Gui.class
				.getResource("/potentialgames/next.png")));
		nextbtn.setIcon(new ImageIcon(Gui.class
				.getResource("/potentialgames/next.png")));
		nextbtn.setToolTipText("Click to switch player");
		nextbtn.setBorderPainted(false);
		nextbtn.setContentAreaFilled(false);
		nextbtn.setBounds(Numbers.ZEHN, Numbers.SECHSHUNDERTNEUNUNDZWANZIG,
				Numbers.FUENFZIG, Numbers.FUENFZIG);
		nextbtn.addActionListener(this);
		panel.add(nextbtn);

		logo = new JButton("");
		logo.setBackground(Color.ORANGE);
		logo.setBounds(Numbers.DREIHUNDERTSIEBENSUNDSECHSIG,
				Numbers.ZWEIHUNDERTACHTUNDFUENZIG,
				Numbers.ZWEIHUNDERTFUENFUNDNEUNZIG, Numbers.ZWEIHUNDERTZWANZIG);
		logo.setSelectedIcon(new ImageIcon(Gui.class
				.getResource("/potentialgames/logo.png")));
		logo.setToolTipText("MADN ROCKS");
		logo.setIcon(new ImageIcon(Gui.class
				.getResource("/potentialgames/logo.png")));
		logo.setPreferredSize(new Dimension(Numbers.FUENFZIG, Numbers.FUENFZIG));
		logo.setBorderPainted(false);
		logo.setContentAreaFilled(false);
		logo.addActionListener(this);
		panel.add(logo);

		createHomes();
		createTargets();

	}

	public void createHomes() {
		if (gc.getPlayercount() > 0) {
			home1 = new JButton("P1 - " + gc.getField().getHome(0).size());
			home1.setFont(new Font(font, Font.BOLD, Numbers.ACHZEN));
			home1.setBounds(Numbers.ACHTHUNDERTVIERUNDSECHZIG,
					Numbers.FUENFHUNDERFUENFZIG, Numbers.NEUNZIG,
					Numbers.NEUNZIG);
			home1.setBackground(Color.RED);
			home1.setForeground(Color.GRAY);
			home1.setBorderPainted(false);
			home1.addActionListener(this);
			panel.add(home1);
		}

		if (gc.getPlayercount() > 1) {
			home2 = new JButton("P2 - " + gc.getField().getHome(1).size());
			home2.setForeground(Color.GRAY);
			home2.setFont(new Font(font, Font.BOLD, Numbers.ACHZEN));
			home2.setBounds(Numbers.HUNDERT, Numbers.FUENFHUNDERFUENFZIG,
					Numbers.NEUNZIG, Numbers.NEUNZIG);
			home2.setBackground(Color.YELLOW);
			home2.setForeground(Color.GRAY);
			home2.setBorderPainted(false);
			home2.addActionListener(this);
			panel.add(home2);
		}

		if (gc.getPlayercount() > 2) {
			home3 = new JButton("P3 - " + gc.getField().getHome(2).size());
			home3.setForeground(Color.GRAY);
			home3.setFont(new Font(font, Font.BOLD, Numbers.ACHZEN));
			home3.setBounds(Numbers.HUNDERT, Numbers.HUNDERT, Numbers.NEUNZIG,
					Numbers.NEUNZIG);
			home3.setBackground(Color.BLUE);
			home3.setForeground(Color.GRAY);
			home3.setBorderPainted(false);
			home3.addActionListener(this);
			panel.add(home3);
		}

		if (gc.getPlayercount() > Numbers.DREI) {
			home4 = new JButton("P4 - " + gc.getField().getHome(Numbers.DREI).size());
			home4.setForeground(Color.GRAY);
			home4.setFont(new Font(font, Font.BOLD, Numbers.ACHZEN));
			home4.setBounds(Numbers.ACHTHUNDERTVIERUNDSECHZIG, Numbers.HUNDERT,
					Numbers.NEUNZIG, Numbers.NEUNZIG);
			home4.setBackground(Color.GREEN);
			home4.setForeground(Color.GRAY);
			home4.setBorderPainted(false);
			home4.addActionListener(this);
			panel.add(home4);
		}
	}

	public void createTargets() {
		if (gc.getPlayercount() > 0) {
			target1 = new JButton("P1 Target - "
					+ gc.getField().getTarget(0).size());
			target1.setFont(new Font(font, Font.BOLD, Numbers.ELF));
			target1.setBounds(Numbers.SIEBENHUNDERTZEHN,
					Numbers.VIERHUNDERTFUENFZEHN, Numbers.NEUNZIG,
					Numbers.NEUNZIG);
			target1.setBackground(Color.RED);
			target1.setForeground(Color.GRAY);
			target1.setBorderPainted(false);

			panel.add(target1);
		}

		if (gc.getPlayercount() > 1) {
			target2 = new JButton("P2 Target - "
					+ gc.getField().getTarget(1).size());
			target2.setForeground(Color.GRAY);
			target2.setFont(new Font(font, Font.BOLD, Numbers.ELF));
			target2.setBounds(Numbers.ZWEIHUNDERTFUENZIG,
					Numbers.VIERHUNDERTFUENFZEHN, Numbers.NEUNZIG,
					Numbers.NEUNZIG);
			target2.setBackground(Color.YELLOW);
			target2.setForeground(Color.GRAY);
			target2.setBorderPainted(false);

			panel.add(target2);
		}

		if (gc.getPlayercount() > 2) {
			target3 = new JButton("P3 Target - "
					+ gc.getField().getTarget(2).size());
			target3.setForeground(Color.GRAY);
			target3.setFont(new Font(font, Font.BOLD, Numbers.ELF));
			target3.setBounds(Numbers.ZWEIHUNDERTFUENZIG,
					Numbers.ZWEIHUNDERTZWANZIG, Numbers.NEUNZIG,
					Numbers.NEUNZIG);
			target3.setBackground(Color.BLUE);
			target3.setForeground(Color.GRAY);
			target3.setBorderPainted(false);

			panel.add(target3);
		}

		if (gc.getPlayercount() > Numbers.DREI) {
			target4 = new JButton("P4 Target - "
					+ gc.getField().getTarget(Numbers.DREI).size());
			target4.setForeground(Color.GRAY);
			target4.setFont(new Font(font, Font.BOLD, Numbers.ELF));
			target4.setBounds(Numbers.SIEBENHUNDERTZEHN,
					Numbers.ZWEIHUNDERTZWANZIG, Numbers.NEUNZIG,
					Numbers.NEUNZIG);
			target4.setBackground(Color.GREEN);
			target4.setForeground(Color.GRAY);
			target4.setBorderPainted(false);

			panel.add(target4);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		msg.setText((String) arg);
		redraw(arg);
	}
}
